(function(){
    const checkbox_menu = document.querySelector('#toggle_menu');
    const container_menu = document.querySelector('#container_menu');
    const container_header = document.querySelector('#container_header');
    const container_header_link = document.getElementsByClassName('menu-link');
    /*const logo name*/
    const logo_name = document.querySelector('#logo_name');
    /*const icons close and menu*/
    const icon_close = document.querySelector('#icon-close');
    const icon_menu = document.querySelector('#icon-menu');

    /*Container menu burguer*/
    const container_menu_burguer = document.querySelector('#container-menu-burguer');
    checkbox_menu.addEventListener('click',function(){
        container_menu.classList.toggle('d-none');

        if (checkbox_menu.checked){
            /*change background from header*/
            container_header.classList.add('bg-secondary');
            container_header.classList.remove('bg-primary');

            /*change color logo name*/
            logo_name.classList.add('color-primary');
            logo_name.classList.remove('color-white');

            /*icon close and show*/
            icon_close.classList.toggle('d-none');
            icon_menu.classList.toggle('d-none');
        }else{
            container_header.classList.add('bg-primary');
            container_header.classList.remove('bg-secondary');

            logo_name.classList.remove('color-primary');
            logo_name.classList.add('color-white');

            icon_close.classList.toggle('d-none');
            icon_menu.classList.toggle('d-none');
        }
    });

    responsive();

    function responsive(){
        if(window.innerWidth >= 800){
            container_menu_burguer.classList.add('d-none');
            container_menu.classList.remove('d-none');
            container_menu.classList.remove('bg-secondary');

            for (const link of container_header_link) {
                link.classList.remove('color-primary');
                link.classList.add('color-white');
            }

            orderCard();

        }else{
            container_menu_burguer.classList.remove('d-none');
            container_menu.classList.add('d-none');
            container_menu.classList.add('bg-secondary');

            for (const link of container_header_link) {
                link.classList.add('color-primary');
                link.classList.remove('color-white');
            }
        }
    }

    window.addEventListener('resize',function (e) {
        responsive();
        heightSections();
    });


    /*Section projects*/
    function orderCard(){
        const container_box_proejcts = document.querySelector('#container-box-proejcts');

        for (let i = 0; i < container_box_proejcts.children.length; i++) {
            const box_left = document.getElementsByClassName('box--left');
            if (i%2 !== 0){
                /*box_left[i].classList.add('order-1');*/
                box_left[i].style.order = '1';
            }

        }
    }

    /*Scroll smooth*/
    const container_hero = document.querySelector('#container-hero');
    const container_about = document.querySelector('#container-about');
    const container_experience = document.querySelector('#container-experience');
    const container_projects = document.querySelector('#container-projects');
    const container_talking = document.querySelector('#container-contact');
    const container_testimonials = document.querySelector('#container-testimonials');

    const logo_go_home = document.querySelector('#logo-go-home');

    logo_go_home.addEventListener('click',function(){
        window.scrollTo({
            top:0,
            behavior:"smooth",
            left:0
        })
    })

    heightSections();

    function heightSections(){
        var heightHero = container_hero.clientHeight;
        var heightAbout = container_about.clientHeight;
        var heightExperience = container_experience.clientHeight;
        var heightProject = container_projects.clientHeight;
        var heightTalking = container_talking.clientHeight;
    }

    document.querySelectorAll('.menu-list a[href^="#"]').forEach((anchor,key) => {

        let heightHero = container_hero.clientHeight;
        let heightAbout = container_about.clientHeight;
        let heightExperience = container_experience.clientHeight;
        let heightTalking = container_talking.clientHeight;
        let heightProject = container_projects.clientHeight;
        let heightTestimonials = container_testimonials.clientHeight;

        anchor.addEventListener('click',function (e){
            e.preventDefault();
            if (key === 0){
                window.scrollTo({
                    top:heightHero,
                    behavior:"smooth",
                    left:0
                })
            }

            if (key === 2){
                window.scrollTo({
                    top:heightAbout + heightHero,
                    behavior:"smooth",
                    left:0
                })
            }

            if (key === 1){
                window.scrollTo({
                    top:heightAbout + heightHero + heightProject,
                    behavior:"smooth",
                    left:0
                })
            }

            if (key === 3){
                window.scrollTo({
                    top:heightAbout + heightHero + heightProject + heightExperience + heightTestimonials,
                    behavior:"smooth",
                    left:0
                })
            }
        })
    });

    /*
    *   Replace url all resources
    *   url : https://eliseovela12.gitlab.io/portafolio-vela/
    * */
    const url = 'https://eliseovela12.gitlab.io/portafolio-vela/';
    let source_image = document.querySelectorAll('img');
    for (let item of source_image) {
        let index_img = item.src.indexOf('img');
        let src_img = item.src.slice(index_img);
        item.src = `${url}${src_img}`;
    }

    const source_css = document.querySelectorAll('link');
    for (let item of source_css) {
        let index_href = item.href.indexOf('css');
        let href_css = item.href.slice(index_href);
        item.href = `${url}${href_css}`;
    }

    const source_script = document.querySelectorAll('script');
    for (let item of source_script) {
        let index_href = item.src.indexOf('js');
        let href_script = item.src.slice(index_href);
        item.src = `${url}${href_script}`;
    }


})();